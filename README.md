 - Message example for topic 1 : {'id':1,'description':'Test Payload'}
 - Require a environment variable "ArnTopic" with the Arn of the topic 2
 - To generate the zip container with all the dependencies:
    ```
    python3 -m venv v-env
    source v-env/bin/activate
    pip install boto3
    deactivate
    cd v-env/lib/python3.6/site-packages/
    zip -r9 ${OLDPWD}/function.zip .
    cd $OLDPWD
    zip -g function.zip lambda_function.py
    ```