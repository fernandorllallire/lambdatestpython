from __future__ import print_function
import ast
import boto3
import os
import logging

logger = logging.getLogger()
logger.setLevel(logging.INFO)


def lambda_handler(event, context):
    try:
        message = ast.literal_eval(event['Records'][0]['Sns']['Message'])
        new_message = {'id': message['id'],
                       'timeStamp': event['Records'][0]['Sns']['Timestamp'],
                       'description': message['description'] + ' Fernando Lambda Python'}
    except KeyError as error:
        logger.error("Problem in the message format : " + str(error))
    except SyntaxError as error:
        logger.error("Problem in the message syntax : " + str(error))
    else:
        sns = boto3.client('sns')
        response = sns.publish(
            TopicArn=os.environ['ArnTopic'],
            Message=str(new_message)
        )
        logger.info(new_message)
    return None
